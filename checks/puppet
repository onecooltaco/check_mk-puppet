#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
# check_puppet - check_mk plugin to Puppet agent
# Copyright (c) 2012 The Real Jeremy Leggat, Arizona, United States. All rights reserved.
# Author: Jeremy Leggat, 2012, <jleggat@asu.edu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation in version 2.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 675 Mass Ave, Cambridge MA 02139, USA or see <http://www.gnu.org/licenses/>
#
# Example output from agent:
# <<<puppet>>>
# 1345169363
# pid	79
# version_config	1345168165
# version_puppet	2.7.18
# time_filebucket	0.000396
# time_config_retrieval	8.81963109970093
# time_file_line	0.000767
# time_exec	2.50626
# time_file	6.315235
# time_package	0.003957
# time_last_run	1345168805
# time_total	17.9335160997009
# time_ssh_authorized_key	0.000529
# time_host	0.001794
# time_service	0.26692
# time_cron	0.001871
# time_sshkey	0.015892
# time_resources	0.000264
# events_success	0
# events_total	0
# events_failure	0
# changes_total	0
# resources_restarted	0
# resources_failed	0
# resources_out_of_sync	0
# resources_skipped	6
# resources_total	218
# resources_scheduled	0
# resources_failed_to_restart	0
# resources_changed	0

# default levels time since last run in minutes.

puppet_default_values = (30, 60)

def inventory_puppet(checkname, info):
	if len(info) > 0:
		return [ (None, 'puppet_default_levels') ]

# the check function (dummy)
def check_puppet(item, params, info):
	this_time = int(info[0][0])
	perfdata = []
  # loop over all output lines of the agent
	for line in info:
		if line[0] == "pid" and line[1].isdigit:
			pid = line[1]
		if line[0] == "time_last_run" and line[1].isdigit:
			last_run = this_time - int(line[1])/60
		if line[0] == "version_config" and line[1].isdigit:
			version_config = int(line[1])
		if line[0] == "version_puppet":
			version_puppet = int(line[1])
		elif line[0].isalpha() and line[1]:
			perfdata.append((line[0],line[1]))

	if not pid:
		return (1, "WARNING - Agent is not running, last run %dM ago" % last_run, perfdata)
	elif last_run > crit:
		return (2, "CRITICAL - Agent has not run in %dM" % last_run, perfdata)
	elif pid.isdigit:
		return (0, "OK - Agent is running, pid is %d, last run %dM ago" % (pid, last_run), perfdata)

	return (3, "Unknown - No info from puppet")

# declare the check to Check_MK
check_info['puppet'] = \
      (check_puppet, "Puppet Agent", 1, inventory_puppet)